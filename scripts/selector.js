//Idea: https://www.geeksforgeeks.org/form-required-attribute-with-a-custom-validation-message-in-html5/
function InvalidMsg(textbox) {
    if (textbox.style.display != 'none') {
        if (textbox.value === '') {
            textbox.setCustomValidity('Please, submit required data');
        } else if (textbox.validity.typeMismatch) {
            textbox.setCustomValidity('Please, provide the data of indicated type');
        }  else if (textbox.validity.rangeUnderflow) {
            textbox.setCustomValidity('Please, provide the data of indicated type');
        } else {
            textbox.setCustomValidity('');
        }
    }
    return true;
}    