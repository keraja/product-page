
$(document).ready(function(){
    $("#delete-product-btn").click(function(){
        let arr = [];
        const data = document.querySelectorAll('input[class="delete-checkbox"]:checked');
        for (let i = 0; i < data.length; i++) {
            arr.push(data[i].getAttribute('name').substring(8));
        }
      $.post("controllers/handleDelete.php", {
          suggestion: arr},
           function(){
             if (arr.length > 0) {
                arr.forEach(item => {
                  let toRemove = document.getElementsByClassName(`${item}`)[0];
                  toRemove.remove();
              });
            }
      });
    });
});
