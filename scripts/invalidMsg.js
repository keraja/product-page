const map1 = new Map();
map1.set("", ["ignore"]);
map1.set("dvd", ["size"]);
map1.set("book", ["weight"]);
map1.set("furniture", ["height", "width", "length"]);

function selector(that) {
  let val = that.value;
  //Just a border style
  document.querySelector(".extra").style.border =
    val != "" ? "1px solid black" : "none";

  let editVal = "if" + val.charAt(0).toUpperCase() + val.slice(1);
  clearAll(map1);

  //Display selected item
  //Conditional is not for handling differences in product types, it's just checking for a present value. I have one empty placeholder value, so it skips over it.
  if (val.length > 0) {
    document.getElementById(editVal).style.display = "block";
    let itemAttributes = map1.get(val);
    itemAttributes.forEach((element) => {
      document.getElementById(element).required = true;
      document.getElementById(element).disabled = false;
    });
  }
}

function clearAll(aMap) {
  for (let [key, value] of  aMap.entries()) {
  //Conditional is not for handling differences in product types, it's just checking for a present value. I have one empty placeholder value, so it skips over it.
    if (key.length > 0) {
      let editKey = "if" + key.charAt(0).toUpperCase() + key.slice(1);
      document.getElementById(editKey).style.display = "none";
      removeRequired(value);
    }
  }
}

function removeRequired(lst) {
  lst.forEach((element) => {
    let inpt = document.getElementById(element);
    inpt.required = false;
    inpt.removeAttribute("oninvalid");
    inpt.removeAttribute("oninput");
    inpt.setAttribute("disabled", "disabled");
  });
}
