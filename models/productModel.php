<?php
include 'dbh.php';
class ProductModel extends Dbh{
    protected function getProducts() {
        $msql = $this->connect();
        
        $sql = "SELECT * FROM product";
        $result = $msql->query($sql);
        $lst = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                array_push($lst, $row);
            }
        } /* else {
            echo "0 results";
        } */
        $msql->close();
        return $lst;
    }

    protected function insertProduct($newObj) {
        $msql = $this->connect();

        /* Prepared statement, stage 1: prepare */
        if (!($stmt = $msql->prepare("INSERT IGNORE INTO product (sku, name, price, type, specs) VALUES (?, ?, ?, ?, ?)"))) {
            echo "Prepare failed: (" . $msql->errno . ") " . $msql->error;
        }

        /* Prepared statement, stage 2: bind and execute */
        $sku = $newObj->getSKU();
        $name = $newObj->getName();
        $price = $newObj->getPrice();
        $type = $newObj->getType();
        $specs = $newObj->getSpecs();
        if (!$stmt->bind_param("ssdss", $sku, $name, $price, $type, $specs)) {
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->close();
        $msql->close();
    }

    protected function removeProduct($id) {
        $msql = $this->connect();

        /* Prepared statement, stage 1: prepare */
        if (!($stmt = $msql->prepare("DELETE FROM `product` WHERE id=?"))) {
            echo "Prepare failed: (" . $msql->errno . ") " . $msql->error;
        }

        /* Prepared statement, stage 2: bind and execute */
        $idn = $id;
        if (!$stmt->bind_param("i", $idn)) {
        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->close();
        $msql->close();
    }
}

?>