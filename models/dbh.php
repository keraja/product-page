<?php

class Dbh {
    private $host = "localhost";
    private $user = "root";
    private $pwd = "";
    private $dbName = "test";

    protected function connect() {
        // Create connection
        if ('::1' == $_SERVER['REMOTE_ADDR'] || '127.0.0.1' == $_SERVER['REMOTE_ADDR']) {
            //Local server configuration. These are for local test database only so right now I do not care putting it up to Git,
            define('DB_HOST', 'localhost');
            define('DB_USER', 'root');
            define('DB_PASS', '');
            define('DB_BASE', 'test');
        } else {
            //Live server configuration
            define('DB_HOST', 'localhost');
            define('DB_USER', 'id17540207_user');
            define('DB_PASS', 'Password_for_db1');
            define('DB_BASE', 'id17540207_test');
        }

        static $con;
        //$conn = mysqli_connect($this->host, $this->user, $this->pwd, $this->dbName);
        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_BASE);

        // Check connection
        if ($conn -> connect_error) {
            die("Connection error: ") . $conn -> connect_error;
        }
        return $conn;
    }
}

?>