<?php
include_once '../controllers/productController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/fstyle.css">
    <script src="../scripts/selector.js"></script>
    <script src="../scripts/invalidMsg.js"></script>
    <title>Add Product</title>
</head>
<body>
    <div class="wrapper">
    <div class="container">
        <form action="../controllers/handleForm.php" method="POST" id="product_form">
            <div class="total">
                <div class="main">
                    <div class="col1">
                        <label for="sku">SKU</label>
                    </div>
                    <div class="col2">
                        <input type="text" name="sku" id="sku" oninvalid="InvalidMsg(this);" 
                    oninput="InvalidMsg(this);" minlength="1" placeholder="SKU" required>
                    </div>
            
                    <div class="col1"><label for="name">Name</label></div>
                    <div class="col2">
                        <input type="text" name="name" id="name" oninvalid="InvalidMsg(this);" 
                        oninput="InvalidMsg(this);" minlength="1" placeholder="Name" required>
                    </div>
            
                    <div class="col1"><label for="sku">Price ($)</label></div>
                    <div class="col2">
                        <input name="price" id="price"  oninvalid="InvalidMsg(this);" 
                        oninput="InvalidMsg(this);" type="number" min="0" step="0.01" 
                        placeholder="Price ($)" required>
                    </div>
        
                    <div class="col1"><label for="type">Type Switcher</label></div>
                    <div class="col2">
                        <select name="type" id="productType" oninvalid="InvalidMsg(this);" 
                        oninput="InvalidMsg(this);" required onchange="selector(this);">
                        <option value="" selected></option>
                        <option value="dvd">DVD</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                    </div>
                </div>
            </div>

            <div class="extra">
                <div class="ifDvd" id="ifDvd">
                    <h3>DVD section</h3>
                    <p>“Please, provide size”</p>
                    <div class="col1"><label for="dvdSize">Size (MB)</label></div>
                    <div class="col2">
                        <input type="number" min="0" step="0.01" name="size" id="size"  placeholder="Size">
                    </div>
                </div>
                <div class="ifBook" id="ifBook">
                    <h3>Book section</h3>
                    <p>“Please, provide weight”</p>
                    <div class="col1"><label for="weight">Weight (KG)</label></div>
                    <div class="col2">
                        <input type="number" min="0" step="0.01" name="weight" id="weight">
                    </div>
                </div>
                <div class="ifFurniture" id="ifFurniture">
                    <h3>Furniture section</h3>
                    <p>“Please, provide dimensions(HxWxL)”</p>
                    <div class="col1"><label for="height">Height (CM)</label></div>
                    <div class="col2"><input type="number" min="0" step="0.01" name="height" id="height"><br><br>
                    </div>
                    
                    <div class="col1"><label for="width">Width (CM)</label></div>
                    <div class="col2">
                        <input type="number" min="0" step="0.01" name="width" id="width"><br><br>
                    </div>

                    <div class="col1"><label for="length">Length (CM)</label></div>
                    <div class="col2">
                        <input type="number" min="0" step="0.01" name="length" id="length">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn saveBtn" name="submit">Save</button>
            <button class="btn cancelBtn" onclick="event.preventDefault(); location='../index.php'">Cancel</button>
        </form>
    </div>
    </div>
</body>
</html>