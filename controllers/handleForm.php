<?php

require_once 'productController.php';
require_once 'product.abstract.php';
require_once 'dvd.class.php';
require_once 'book.class.php';
require_once 'furniture.class.php';

class HandleForm extends ProductController {
    public function __construct() {
        $newOb = (object)[];
        $sku = filter_var($_POST['sku'], FILTER_SANITIZE_STRING); //From example thought had to use uppercase strtoupper() but not
        $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
        $price = number_format(filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2);
        $type = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
        
        //If is dvd, set specs to dvd specs
        $newOb = isset($_POST['size']) && $_POST['size'] != "" ? 
        new DVD($sku, $name, $price, $type, "Size: " 
        . number_format(filter_var($_POST['size'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2) . " MB") 
        : $newOb;

      //If is book, set specs to book specs
      $newOb = isset($_POST['weight']) && $_POST['weight'] != "" ? 
        new Book($sku, $name, $price, $type, "Weight: " 
        . number_format(filter_var($_POST['weight'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2) . " KG") 
        : $newOb;

      //If is furniture, set specs to furniture specs
      $newOb = isset($_POST['height']) && $_POST['height'] != "" ? 
        new Furniture($sku, $name, $price, $type, "Dimensions: " 
        . number_format(filter_var($_POST['height'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2)
        . "x" . number_format(filter_var($_POST['width'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2) 
        . "x" . number_format(filter_var($_POST['length'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION), 2)) 
        : $newOb;
        
        $tst = new ProductController();
        $tst->addProduct($newOb);
    }
}

new Handleform();
?>