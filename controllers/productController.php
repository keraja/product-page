<?php
///test_task2/index.php locally
if ($_SERVER['SCRIPT_NAME'] == "/index.php") {
    include 'models/productModel.php';
} else {
    include '../models/productModel.php';
}

class ProductController extends ProductModel {
    public function getProd() {
        foreach ($this->getProducts() as $crd) {
            echo
                '<div class="product-card '. $crd["id"] .'">
                    <input type="checkbox" class="delete-checkbox" name="checkbox'.$crd["id"].'">
                    <div class="description-list">
                        <p class="description">'.$crd["sku"].'</p>
                        <p class="description">'.$crd["name"].'</p>
                        <p class="description">'.$crd["price"].' $</p>
                        <p class="description">'.$crd["specs"].'</p>
                    </div>
                </div>';
        }
        //return $this->getProducts();
    }

    public function addProduct($newObj) {
        $newOb = $newObj;
        $this->insertProduct($newObj);
        header('Location: ../index.php');
        return;
    }

    public function massDelete($ids) {
        foreach ($ids as $item) {
            $this->removeProduct(intval($item));
        }
    }
}

?>