<?php
require_once 'productController.php';

class HandleDelete extends ProductController {
    public function __construct() {
        if (isset($_POST['suggestion'])) {
            $ids = array();
            $tst = new ProductController();
            foreach ($_POST['suggestion'] as $item) {
                if (is_numeric($item)) {
                    $item = number_format($item);
                    if (!filter_var($item, FILTER_VALIDATE_INT) === false) {
                        array_push($ids, $item);
                    }
                }
            }
            $tst->massDelete($ids);
        }
    }
}

new HandleDelete();
?>