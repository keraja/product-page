<?php
declare(strict_types=1);

abstract class Product {
    protected $id;
    public $sku;
    public $name;
    public $price;
    public $type;
    public $specs;

    public function __construct($sku2, $name2, $price2, $type2, $specs2) {
        $this->sku = $sku2;
        $this->name = $name2;
        $this->price = $price2;
        $this->type = $type2;
        $this->specs = $specs2;
    }
    
    public function getSKU() {
        return $this->sku;
    }

    public function setSKU($param) {
        $this->sku = $param;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($param) {
        $this->name = $param;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($param) {
        $this->price = $param;
    }

    protected function priceToString() {
        return $this->price . " $";
    }

    public function getType() {
        return $this->type;
    }

    public function setType($param) {
        $this->type = $param;
    }

    public function getSpecs() {
        return $this->specs;
    }

    abstract public function setSpecs($param, $param1, $param2);

    public function asList() {
        return [$this->getSKU(), $this->getName(), $this->getPrice(), $this->getType(), $this->getSpecs()];
    }
}

?>