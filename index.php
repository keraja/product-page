<?php
include 'controllers/productController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles/styles.css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="scripts/getChecked.js"></script>
        <title>Product List</title>
</head>
<body>
    <div class="container">
        <nav class="navbar">
            <p>Product List</p>
            <ul>
                <li><button onclick="location='views/addProductView.php'">ADD</button></li>
                <li><button id="delete-product-btn">MASS DELETE</button></li>
            </ul>
        </nav>
        <div class="short-border"></div>
        <div class="projects-grid">
            <?php
                $tst = new ProductController();
                $tst = $tst->getProd();
            ?>
        </div>
    </div>
    <footer>
            <div class="short-border"></div>
            <p>ScandiWeb Test Assignment</p>
            <div>
                <p>By: Kevin Raja</p>
                <p>Email: kkevinraja@gmail.com</p>
            </div>
    </footer>
</body>
</html>